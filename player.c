/*Galen Leake
  CS46 Fall 2016
  Agar Project*/

#include "bot.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>


double myx = 0;//used to hold current x position
double myy = 0;//used to hold current y position


int dist(const void *a, const void *b)
{
    double ax = (*(struct food *)a).x;
    double ay = (*(struct food *)a).y;
    double bx = (*(struct food *)b).x;
    double by = (*(struct food *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}


int distPlayer(const void *a, const void *b){
    double ax=((struct player *)a)->x;
    double ay=((struct player *)a)->y;
    double bx=((struct player *)b)->x;
    double by=((struct player *)b)->y;

    //Distance from me to a
    double dista= sqrt((myx-ax) * (myx-ax) + (myy-ay) * (myy - ay));

    //Distance from me to b
    double distb = sqrt((myx - bx) * (myx-bx) + (myy - by) * (myy -by));

    //Subtract dista-distb
    return dista-distb;
}

int distMass(const void *a, const void *b){

   double ax=((struct player *)a)->totalMass;
   double bx=((struct player *)b)->totalMass;

   //Distance from me to a
   double dista= sqrt((myx-ax) * (myx-ax));
   //Distance from me to b
   double distb = sqrt((myx - bx) * (myx-bx));

   //Subtract dista-distb
   return dista - distb;
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    
    //Player's x-y to globals
    myx = me.x;
    myy = me.y;
    
    // Sort food by distance
    qsort(foods, nfood, sizeof(struct food), dist);
    qsort(players, nplayers, sizeof (struct player), distPlayer);
    qsort(players, nplayers, sizeof (struct player), distMass);
    
    // Move toward closest food
    if (nfood > 0){
        act.dx = foods[0].x - me.x;
        act.dy = foods[0].y - me.y;
        act.fire = 0;
        act.split = 0;  
    }
    
    else if(nplayers > 0){
        //if player smaller then eat them
        if (me.totalMass * .60 < players[0].totalMass){
            act.dx = (players[0].x - me.x) * 100;
            act.dy = (players[0].y -me.y) * 100;
            act.fire = 0;
            act.split = 0;
        }
        //run away if player is greater in size
        else if (me.totalMass * .85 < players[0].totalMass){
            act.dx = (players[0].x + me.x) * 100;
            act.dy = (players[0].y + me.y) * 100;
            act.fire = 1;
            act.split = 1;
        }
        
    }
    //If there is both players and food nearby, split and run
    else if(nfood > 0 && nplayers > 0){
        act.dx = players[0].x + me.x;
        act.dy = players[0].y + me.y;
        act.fire = 0;
        act.split = 1;
    }
    //With no other players nearby, move to lower right to look for food.
    else{
        //Move to the lower-right
        act.dx = 100;
        act.dy = 100;
        act.fire = 0;
        act.split = 1;
    }
    
    return act;
}